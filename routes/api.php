<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'projects','namespace'=>'Projects','as'=>'projects.'], function(){
	Route::get('/','ProjectsController@index')->name('index');
	Route::get('/select','ProjectsController@select')->name('select');
	Route::post('store','ProjectsController@store')->name('store');
	Route::get('/edit/{id}','ProjectsController@edit')->name('edit');
	Route::patch('/update/{id}','ProjectsController@update')->name('update');
	Route::delete('/destroy/{id}','ProjectsController@destroy')->name('destroy');
});

Route::group(['prefix'=>'tasks','namespace'=>'Tasks','as'=>'tasks.'], function(){
	Route::get('/','TaskController@index')->name('index');
	Route::post('/store','TaskController@store')->name('store');
	Route::get('/edit/{id}','TaskController@edit')->name('edit');
	Route::patch('/update/{id}','TaskController@update')->name('update');
	Route::delete('/destroy/{id}','TaskController@destroy')->name('destroy');
});
