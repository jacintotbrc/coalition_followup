import Vue from 'vue'

export const Task = Vue.component('tasks',require('./Task'))
export const TForm = Vue.component('task-form',require('./Form'))