import {Task, TForm} from './../components'

export default [
	{
		path:'/',
		name:'tasks',
		component:Task,
		meta:{
			guest:false,
			needsAuth:false
		}
	},
	{
		path:'/task-form',
		name:'task-form',
		component:TForm,
		meta:{
			guest:false,
			needsAuth:false
		}
	},

	{
		path:'/task-form/:id',
		name:'task-form-update',
		component:TForm,
		meta:{
			guest:false,
			needsAuth:false
		}
	}
]