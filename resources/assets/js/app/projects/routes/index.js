import {Project, PForm} from './../components'

export default [
	{
		path:'/projects',
		name:'projects',
		component:Project,
		meta:{
			guest:false,
			needsAuth:false
		}
	},
	{
		path:'/projects/form',
		name:'project-form',
		component:PForm,
		meta:{
			guest:false,
			needsAuth:false
		}
	}
]