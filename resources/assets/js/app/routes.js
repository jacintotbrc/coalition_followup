import tasks from './tasks/routes'
import projects from './projects/routes'

export default [...tasks,...projects]