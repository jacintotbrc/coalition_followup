import router from './router'
import store from './vuex'
import vSelect from 'vue-select'
import VuejsDialog from "vuejs-dialog"
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('coalition-app', require('./components/Coalition.vue'));
Vue.component('navigation', require('./components/Nav.vue'));

/*NPM Helpers*/
Vue.component('v-select', vSelect)
Vue.use(VuejsDialog)

const app = new Vue({
    router:router,
    store:store,
    el: '#coalition'
});
