<?php

namespace App\Http\Controllers\Projects;

use App\Models\Project as Projects;
use Illuminate\Http\Request;
use App\Http\Requests\Projects\ProjectFormRequest;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projects::get();

        return response()->json([
            'data'  =>$projects
        ],201);
    }

    public function select()
    {
        $projects = Projects::select('id','name')->get();

        return response()->json([
            'data'  =>$projects
        ],201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectFormRequest $request)
    {
        $project = new Projects;

        $project->name = $request->get('name');
        $project->save();

        if($project)
            return response()->json([
                'data'  =>$project
            ],201);
        else
            return response()->json([
                'errorMsg' =>'Something went wrong'
            ],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Projects::findOrFail($id);

        return response()->json([
            'data'  =>$project
        ],201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectFormRequest $request, $id)
    {
        $project = Projects::findOrFail($id);
        $project->name = $request->get('name');
        $project->save();

        return response()->json(['data'=>$project],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $project = Projects::findOrFail($id);
       $project->delete();
       return response()->json(null,201); 
    }
}
